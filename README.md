This project aims to make a custom breakout board for the GD32VF103 in the formfactor of the Arduino Pro Mini.
Specifically the 3.3v version of the Arduino Pro Mini.

![3D Render](riscvduino.png "3D Render")
